nanoagent
=========

A lightweight wrapper for http(s) requests. Redirects and compressed streams are handled transparently.

## Installation

  npm install nanoagent

## Usage

  var nanoagent = require('nanoagent');

  nanoagent.queue('http://domain.com/path', function(result){

    console.log(result.body);

  });

  // result:{url, red, err, statusCode, headers, body}

## TODO

	- options for individual requests
	- cookie handling