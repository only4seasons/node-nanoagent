/**
	none-nanoagent
*/
var mod_http 		= require('http'),
		mod_https 	= require('https'),
		mod_url 		= require('url'),
		mod_zlib		= require('zlib'),
		mod_util		= require('util');
		mod_stream	= require('stream');

var	MAXRED = 3, 			// max redirects
		MAXREQ = 10,			// max concurrent requests
		MAXLEN = 64*1024,	// max body length
		numreq = 0;				// active requests

var queuedRequests = [];

var optHeaders = {
	'User-Agent': 'nanoagent',
	'Accept-Encoding': 'gzip,deflate'
};

exports.queue = function(url, callback) {
	queuedRequests.push({
		url: url,
		cbk: callback,
		red: 0,
		err: false,
		statusCode: 0,
		headers: [],
		body: null
	});
	processNext();
}

function processNext() {
	if (numreq>=MAXREQ || queuedRequests.length==0) return;
	var o = queuedRequests.shift();
	
	var opt = mod_url.parse(o.url);
	var mod = null;
	switch (opt.protocol) {
		case 'http:': mod = mod_http; break;
		case 'https:': mod = mod_https; break;
		default:
			o.err = true;
			endRequest(o);
			return;
	}
	opt.headers = optHeaders;
	var req = mod.get(opt, onResponse);
	req.on('error', onError);
	req.on('close', onClose);
	req.nanoagent = o;
	numreq++;
}

function onResponse(res) {
	var o = this.nanoagent;
	res.nanoagent = o;
	o.statusCode = res.statusCode;
	o.headers = res.headers;
	o.body = new StrWStream();
	o.body.nanoagent = o;
	if (res.statusCode>=200 && res.statusCode<300) {
		var mod = null;
		switch (res.headers['content-encoding']) {
	    case 'gzip':  	mod = mod_zlib.createGunzip(); break;
	    case 'deflate': mod = mod_zlib.createInflate(); break;
	    default: break;
	  }
	  if (mod) {
	  	mod.on('error', onError);
	  	mod.nanoagent = o;
	  	res.pipe(mod).pipe(o.body);
	  }
	  else
	  	res.pipe(o.body);
	  o.body.on('finish', onFinish);
		return;
	}
	process.nextTick(processNext);
	if (res.statusCode>=300 && res.statusCode<400) {
		var new_url = mod_url.resolve(o.url, res.headers['location']);
		if (++o.red <= MAXRED) {
			this.redirect = true;
			o.url = new_url;
			queuedRequests.push(o);
			o.body.removeListener('finish', onFinish);
			o.body = null;
			o.headers = [];
			return;
		}
	}
	o.err = true;
	endRequest(o);
}

function onError(e) {
	this.nanoagent.err = true;
	if (this.nanoagent.body)
		this.nanoagent.body.end();
}

function onClose() {
	numreq--;
	if (!this.nanoagent.body && !this.redirect)
		endRequest(this.nanoagent);
}

function onFinish() {
	endRequest(this.nanoagent);
}

function endRequest(o) {
	o.body = o.body?o.body.toString():'';
	process.nextTick(function() {
		o.cbk(o);
	});
	process.nextTick(processNext);
}

function StrWStream(options) {
	mod_stream.Writable.call(this, options);
	this.str = '';
}

mod_util.inherits(StrWStream, mod_stream.Writable);

StrWStream.prototype._write = function(chunk, encoding, callback) {
	this.str += chunk.toString();
	callback();
}

StrWStream.prototype.toString = function() {
	return this.str;
}